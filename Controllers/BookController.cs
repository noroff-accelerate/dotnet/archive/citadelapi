﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TheCitadelAPI.Data;
using TheCitadelAPI.Dtos;
using TheCitadelAPI.Dtos.Book;
using TheCitadelAPI.Models;

namespace TheCitadelAPI.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly DTOParser _dtoParser;
        private readonly IMapper _mapper;

        public BookController(ApplicationDbContext applicationDbContext, DTOParser dtoParser, IMapper mapper)
        {
            _context = applicationDbContext;
            _dtoParser = dtoParser;
            _mapper = mapper;
        }

        // GET: api/book/Sample
        [AllowAnonymous]
        [HttpGet("sample")]
        public async Task<ActionResult<BookReadDTO>> GetSample()
        {
            //#region EagerLoad
            //Random random = new Random();

            //var result = _context.Books
            //            .Include(b => b.BookAuthors)
            //            .ThenInclude(ba => ba.Author)
            //            .ToList();

            //var bookSample = result[random.Next(0, 5)];

            //if (bookSample == null)
            //{
            //    return NotFound();
            //}

            ////The automapper works with fully loaded objects from DBSets. 
            //var bookFromMapper = await Task<BookReadDTO>.Run(() => _mapper.Map<BookReadDTO>(bookSample));
            //#endregion

            //return bookFromMapper;

            return new BookReadDTO() { Title = "sample" };
        }

        // GET: api/Book/978-3-16-148410-1
        [HttpGet("{isbn}", Name = "Get")]
        public async Task<ActionResult<Book>> Get(string isbn)
        {
            //Easger loading
            var result = _context.Books
                        .Include(b => b.BookAuthors)
                        .ThenInclude(ba => ba.Author)
                        .ToList();

            var bookSample =  await _context.Books.FindAsync(isbn);

            if (bookSample == null)
            {
                return NotFound();
            }
 
            var bookFromMapper = _mapper.Map<BookReadDTO>(bookSample);
            
            return bookSample;
        }

        
        // POST: api/book
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] BookCreateDTO book)
        {

            var allBooks = _context.Books
                       .Include(b => b.BookAuthors)
                       .ThenInclude(ba => ba.Author)
                       .ToList();

            //Verify if book already exists in DB
            var duplicate = allBooks.Where(b=>b.ISBN == book.ISBN).FirstOrDefault();

            if(allBooks.Contains(duplicate))
            {
                return BadRequest();
            }

           #region InsertLogic

            foreach (var author in book.Authors)
            {
                var newAuthor = _mapper.Map<Author>(author);
                //Need to check for existing entries for authors before re-adding
                 _context.Authors.Add(newAuthor);
                
            }

            _context.SaveChanges();

            var newBook = _mapper.Map<Book>(book);

            await _context.Books.AddAsync(newBook);

            foreach (var author in book.Authors)
            {
                //this wont work for authors sharing the same name
                var authorFromDB = _context.Authors.Where(a => a.FirstName == author.FirstName && a.LastName == author.LastName).FirstOrDefault();

                 _context.BookAuthors.Add(
                    new BookAuthor()
                    {
                        ISBN = book.ISBN,
                        AuthorID = authorFromDB.AuthorId
                    }
                    );
            }

            _context.SaveChanges();

            #endregion
           
           var readBook = _mapper.Map<BookReadDTO>(newBook);

            return CreatedAtAction(nameof(Get),new {isbn = book.ISBN },readBook);
        }

        // PUT: api/Book/978-3-16-148410-0
        [HttpPut("{isbn}")]
        public async Task<ActionResult> Put(string isbn, [FromBody] BookUpdateDTO book)
        {
            if (isbn != book.ISBN)
            {
                return BadRequest();
            }

            var oldBook = await _context.Books.FindAsync(isbn);

            _mapper.Map(book, oldBook);

            _context.SaveChanges();

            return NoContent();
        }

        // DELETE: api/Book/978-3-16-148410-0
        [HttpDelete("{isbn}")]
        public async Task<ActionResult> Delete(string isbn)
        {
            var book = await _context.Books.FindAsync(isbn);

            if(book == null)
            {
                return NotFound();
            }

            _context.Books.Remove(book);

            _context.SaveChanges();

            return NoContent();
        }
    }
}
