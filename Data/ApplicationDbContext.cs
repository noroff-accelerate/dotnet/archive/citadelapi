﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheCitadelAPI.Models;

namespace TheCitadelAPI.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Book> Books { get; set; }

        public DbSet<Author> Authors { get; set; }

        public DbSet<BookAuthor> BookAuthors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //BookAuthor Joining table configuration
            modelBuilder.Entity<BookAuthor>().HasKey(ba => new { ba.ISBN, ba.AuthorID });

            modelBuilder.Entity<BookAuthor>().HasOne(b => b.Book).WithMany(ba => ba.BookAuthors).HasForeignKey(ba => ba.ISBN);

            modelBuilder.Entity<BookAuthor>().HasOne(a => a.Author).WithMany(ba => ba.BookAuthors).HasForeignKey(ba => ba.AuthorID);

            //Book table seeding
            #region Book Data Seeding

            var bookSeed = new List<Book>()
            {
                new Book {ISBN = "978-3-16-148410-0" , Title = "Lord of the rings",Genre = "Fantasy Fiction" },
                new Book {ISBN = "978-3-16-148410-1" , Title = "Brave New World", Genre = "Science Fiction" },
                new Book {ISBN = "978-3-16-148410-2" , Title = "Thinking Fast and Slow", Genre = "Psychology Non-Fiction" },
                new Book {ISBN = "978-3-16-148410-3" , Title = "Lord of the Flies",Genre = "Litarary Fiction" },
                new Book {ISBN = "978-3-16-148410-4" , Title = "The Game",Genre = "Sociology Non-fiction" },
                new Book {ISBN = "978-3-16-148410-5" , Title = "The Power of Now",Genre = "Esoteric" }
            };

            modelBuilder.Entity<Book>().HasData(bookSeed);

            #endregion

            //Author table seeding
            #region Author Data Seeding

            var authorSeed = new List<Author>()
            {
                 new Author { AuthorId = 1, FirstName = "J. R. R.", LastName = "Tolkien"},
                 new Author { AuthorId = 2, FirstName = "Aldous", LastName = "Huxley"},
                 new Author { AuthorId = 3, FirstName = "Daniel", LastName = "Kahneman"},
                 new Author { AuthorId = 4, FirstName = "Amos", LastName = "Tversky"},
                 new Author { AuthorId = 5, FirstName = "William", LastName = "Golding"},
                 new Author { AuthorId = 6, FirstName = "Neil", LastName = "Strauss"},
                 new Author { AuthorId = 7, FirstName = "Eckard", LastName = "Tolle"},
            };

            modelBuilder.Entity<Author>().HasData(authorSeed);

            #endregion



            //BookAuthor table seeding
            #region Author Data Seeding

            var bookauthorSeed = new List<BookAuthor>()
            {
                new BookAuthor {ISBN = "978-3-16-148410-0", AuthorID = 1},
                new BookAuthor {ISBN = "978-3-16-148410-1", AuthorID = 2},
                new BookAuthor {ISBN = "978-3-16-148410-2", AuthorID = 3},
                new BookAuthor {ISBN = "978-3-16-148410-2", AuthorID = 4},
                new BookAuthor {ISBN = "978-3-16-148410-3", AuthorID = 5},
                new BookAuthor {ISBN = "978-3-16-148410-4", AuthorID = 6},
                new BookAuthor {ISBN = "978-3-16-148410-5", AuthorID = 7}
            };

            modelBuilder.Entity<BookAuthor>().HasData(bookauthorSeed);

            #endregion

        }





    }
}
