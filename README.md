# The Citadel

## Getting Started

Clone to a local directory.

Open solution in Visual Studio

Run Migrations

Run

## Payload Format

Book

{
"isbn":"978-3-16-148410-2",
"title":"Thinking Fast and Slow",
"genre":"Psychology Non-Fiction",
"authors":["Daniel Kahneman","Amos Tversky"]
}

## Endpoints

URI		Verb	Operation	Success		Failure
api/book/sample	GET	READ		200 OK 		400 Bad Request
							404 Not Found
api/book/{isbn}	GET	READ		200 OK		400 Bad Request
							404 Not Found
api/book	POST	CREATE		201 Created	400 Bad Request
							405 Not Allowed
api/book/{isbn}	PUT	UPDATE		204 No content	400 Bad Request
							405 Not Allowed
api/book/{isbn}	DELETE	DELETE		200 OK		204 No content	
							400 Bad Request
							405 Not Allowed


### Prerequisites

.NET Framework

.NET Core 3.1

Visual Studio 2017/19 OR Visual Studio Code


## Authors

***Dean von Schoultz** [deanvons](https://gitlab.com/deanvons)




