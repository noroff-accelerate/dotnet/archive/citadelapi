﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheCitadelAPI.Data;

namespace TheCitadelAPI.Models
{
    public class DTOParser
    {
        //Converts a Book Model instance into a DTO for consumption without navigation properties used by EF
        //This is an alternative to the automapper
        public BookReadDTO ParseBook(Book book)
        {
            List<string> Authors = new List<string>();

            foreach (var bookAuthor in book.BookAuthors)
            {
                Authors.Add($"{bookAuthor.Author.FirstName} {bookAuthor.Author.LastName}");
            }

            return new BookReadDTO
            {
                ISBN = book.ISBN,
                Title = book.Title,
                Genre = book.Genre,
                Authors = Authors.ToArray<string>()
            };
        }
    }
}
