﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheCitadelAPI.Models
{
    public static class BookSampleDataRepo
    {

        public static List<Book> SampleBookData { get; set; } = new List<Book> {
            new Book {ISBN = "978-3-16-148410-0" , Title = "Lord of the rings",Genre = "Fantasy Fiction" },
            new Book {ISBN = "978-3-16-148410-1" , Title = "Brave New World", Genre = "Science Fiction" },
            new Book {ISBN = "978-3-16-148410-2" , Title = "Thinking Fast and Slow", Genre = "Psychology Non-Fiction" }
            };

        public static List<Author> SampleAuthorData { get; set; } = new List<Author> {
            new Author { AuthorId = 1, FirstName = "J. R. R.", LastName = "Tolkien"},
            new Author { AuthorId = 2, FirstName = "Aldous", LastName = "Huxley"},
            new Author { AuthorId = 3, FirstName = "Daniel", LastName = "Kahneman"},
            new Author { AuthorId = 4, FirstName = "Amos", LastName = "Tversky"}
            };

        public static List<BookAuthor> SampleAuthorBookData { get; set; } = new List<BookAuthor> {
            new BookAuthor {ISBN = "978-3-16-148410-0", AuthorID = 1},
            new BookAuthor {ISBN = "978-3-16-148410-1", AuthorID = 2},
            new BookAuthor {ISBN = "978-3-16-148410-2", AuthorID = 3},
            new BookAuthor {ISBN = "978-3-16-148410-2", AuthorID = 4},

            };

    }
}
