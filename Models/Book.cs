﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TheCitadelAPI.Models
{
    public class Book
    {
        [Key]
        [MaxLength(20)]
        [Required]
        public string ISBN { get; set; }

        [MaxLength(200)]
        public string Title { get; set; }

        [MaxLength(150)]
        public string  Genre { get; set; }

        public ICollection<BookAuthor> BookAuthors { get; set; }



    }
}
