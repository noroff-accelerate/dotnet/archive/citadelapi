﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TheCitadelAPI.Models
{
    public class Author
    {
        [Key]
        [Required]
        public int AuthorId { get; set; }
        [MaxLength(150)]
        public string FirstName { get; set; }
        [MaxLength(150)]
        public string LastName { get; set; }

        public ICollection<BookAuthor> BookAuthors { get; set; }


    }
}
