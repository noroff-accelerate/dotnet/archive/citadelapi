﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TheCitadelAPI.Dtos.Book
{
    public class BookUpdateDTO
    {
        [MaxLength(20)]
        [Required]
        public string ISBN { get; set; }

        [MaxLength(200)]
        public string Title { get; set; }

        [MaxLength(150)]
        public string Genre { get; set; }
    }
}
