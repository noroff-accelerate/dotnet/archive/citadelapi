﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheCitadelAPI.Models
{
    public class BookReadDTO
    {
        public string ISBN { get; set; }

        public string Title { get; set; }

        public string Genre { get; set; }

        public string [] Authors { get; set; }

    }
}
