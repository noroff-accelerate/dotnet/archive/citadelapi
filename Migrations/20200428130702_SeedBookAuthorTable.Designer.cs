﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using TheCitadelAPI.Data;

namespace TheCitadelAPI.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20200428130702_SeedBookAuthorTable")]
    partial class SeedBookAuthorTable
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TheCitadelAPI.Models.Author", b =>
                {
                    b.Property<int>("AuthorId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(150)")
                        .HasMaxLength(150);

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(150)")
                        .HasMaxLength(150);

                    b.HasKey("AuthorId");

                    b.ToTable("Authors");

                    b.HasData(
                        new
                        {
                            AuthorId = 1,
                            FirstName = "J. R. R.",
                            LastName = "Tolkien"
                        },
                        new
                        {
                            AuthorId = 2,
                            FirstName = "Aldous",
                            LastName = "Huxley"
                        },
                        new
                        {
                            AuthorId = 3,
                            FirstName = "Daniel",
                            LastName = "Kahneman"
                        },
                        new
                        {
                            AuthorId = 4,
                            FirstName = "Amos",
                            LastName = "Tversky"
                        },
                        new
                        {
                            AuthorId = 5,
                            FirstName = "William",
                            LastName = "Golding"
                        },
                        new
                        {
                            AuthorId = 6,
                            FirstName = "Neil",
                            LastName = "Strauss"
                        },
                        new
                        {
                            AuthorId = 7,
                            FirstName = "Eckard",
                            LastName = "Tolle"
                        });
                });

            modelBuilder.Entity("TheCitadelAPI.Models.Book", b =>
                {
                    b.Property<string>("ISBN")
                        .HasColumnType("nvarchar(20)")
                        .HasMaxLength(20);

                    b.Property<string>("Genre")
                        .HasColumnType("nvarchar(150)")
                        .HasMaxLength(150);

                    b.Property<string>("Title")
                        .HasColumnType("nvarchar(200)")
                        .HasMaxLength(200);

                    b.HasKey("ISBN");

                    b.ToTable("Books");

                    b.HasData(
                        new
                        {
                            ISBN = "978-3-16-148410-0",
                            Genre = "Fantasy Fiction",
                            Title = "Lord of the rings"
                        },
                        new
                        {
                            ISBN = "978-3-16-148410-1",
                            Genre = "Science Fiction",
                            Title = "Brave New World"
                        },
                        new
                        {
                            ISBN = "978-3-16-148410-2",
                            Genre = "Psychology Non-Fiction",
                            Title = "Thinking Fast and Slow"
                        },
                        new
                        {
                            ISBN = "978-3-16-148410-3",
                            Genre = "Litarary Fiction",
                            Title = "Lord of the Flies"
                        },
                        new
                        {
                            ISBN = "978-3-16-148410-4",
                            Genre = "Sociology Non-fiction",
                            Title = "The Game"
                        },
                        new
                        {
                            ISBN = "978-3-16-148410-5",
                            Genre = "Esoteric",
                            Title = "The Power of Now"
                        });
                });

            modelBuilder.Entity("TheCitadelAPI.Models.BookAuthor", b =>
                {
                    b.Property<string>("ISBN")
                        .HasColumnType("nvarchar(20)");

                    b.Property<int>("AuthorID")
                        .HasColumnType("int");

                    b.HasKey("ISBN", "AuthorID");

                    b.HasIndex("AuthorID");

                    b.ToTable("BookAuthors");

                    b.HasData(
                        new
                        {
                            ISBN = "978-3-16-148410-0",
                            AuthorID = 1
                        },
                        new
                        {
                            ISBN = "978-3-16-148410-1",
                            AuthorID = 2
                        },
                        new
                        {
                            ISBN = "978-3-16-148410-2",
                            AuthorID = 3
                        },
                        new
                        {
                            ISBN = "978-3-16-148410-2",
                            AuthorID = 4
                        },
                        new
                        {
                            ISBN = "978-3-16-148410-3",
                            AuthorID = 5
                        },
                        new
                        {
                            ISBN = "978-3-16-148410-4",
                            AuthorID = 6
                        },
                        new
                        {
                            ISBN = "978-3-16-148410-5",
                            AuthorID = 7
                        });
                });

            modelBuilder.Entity("TheCitadelAPI.Models.BookAuthor", b =>
                {
                    b.HasOne("TheCitadelAPI.Models.Author", "Author")
                        .WithMany("BookAuthors")
                        .HasForeignKey("AuthorID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("TheCitadelAPI.Models.Book", "Book")
                        .WithMany("BookAuthors")
                        .HasForeignKey("ISBN")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
