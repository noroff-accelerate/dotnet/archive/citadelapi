﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheCitadelAPI.Migrations
{
    public partial class SeedBookAuthorTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "BookAuthors",
                columns: new[] { "ISBN", "AuthorID" },
                values: new object[,]
                {
                    { "978-3-16-148410-0", 1 },
                    { "978-3-16-148410-1", 2 },
                    { "978-3-16-148410-2", 3 },
                    { "978-3-16-148410-2", 4 },
                    { "978-3-16-148410-3", 5 },
                    { "978-3-16-148410-4", 6 },
                    { "978-3-16-148410-5", 7 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BookAuthors",
                keyColumns: new[] { "ISBN", "AuthorID" },
                keyValues: new object[] { "978-3-16-148410-0", 1 });

            migrationBuilder.DeleteData(
                table: "BookAuthors",
                keyColumns: new[] { "ISBN", "AuthorID" },
                keyValues: new object[] { "978-3-16-148410-1", 2 });

            migrationBuilder.DeleteData(
                table: "BookAuthors",
                keyColumns: new[] { "ISBN", "AuthorID" },
                keyValues: new object[] { "978-3-16-148410-2", 3 });

            migrationBuilder.DeleteData(
                table: "BookAuthors",
                keyColumns: new[] { "ISBN", "AuthorID" },
                keyValues: new object[] { "978-3-16-148410-2", 4 });

            migrationBuilder.DeleteData(
                table: "BookAuthors",
                keyColumns: new[] { "ISBN", "AuthorID" },
                keyValues: new object[] { "978-3-16-148410-3", 5 });

            migrationBuilder.DeleteData(
                table: "BookAuthors",
                keyColumns: new[] { "ISBN", "AuthorID" },
                keyValues: new object[] { "978-3-16-148410-4", 6 });

            migrationBuilder.DeleteData(
                table: "BookAuthors",
                keyColumns: new[] { "ISBN", "AuthorID" },
                keyValues: new object[] { "978-3-16-148410-5", 7 });
        }
    }
}
