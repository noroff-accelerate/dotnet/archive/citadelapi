﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheCitadelAPI.Migrations
{
    public partial class SeedBookTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "ISBN", "Genre", "Title" },
                values: new object[,]
                {
                    { "978-3-16-148410-0", "Fantasy Fiction", "Lord of the rings" },
                    { "978-3-16-148410-1", "Science Fiction", "Brave New World" },
                    { "978-3-16-148410-2", "Psychology Non-Fiction", "Thinking Fast and Slow" },
                    { "978-3-16-148410-3", "Litarary Fiction", "Lord of the Flies" },
                    { "978-3-16-148410-4", "Sociology Non-fiction", "The Game" },
                    { "978-3-16-148410-5", "Esoteric", "The Power of Now" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "ISBN",
                keyValue: "978-3-16-148410-0");

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "ISBN",
                keyValue: "978-3-16-148410-1");

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "ISBN",
                keyValue: "978-3-16-148410-2");

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "ISBN",
                keyValue: "978-3-16-148410-3");

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "ISBN",
                keyValue: "978-3-16-148410-4");

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "ISBN",
                keyValue: "978-3-16-148410-5");
        }
    }
}
