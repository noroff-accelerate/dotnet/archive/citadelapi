﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheCitadelAPI.Dtos.Authors;
using TheCitadelAPI.Models;

namespace TheCitadelAPI.Profiles
{
    public class AuthorProfile : Profile
    {
        public AuthorProfile()
        {
            CreateMap<AuthorCreateDTO, Author>();
        }


    }
}
