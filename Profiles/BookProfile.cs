﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheCitadelAPI.Dtos;
using TheCitadelAPI.Dtos.Book;
using TheCitadelAPI.Models;

namespace TheCitadelAPI.Profiles
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<Book, BookReadDTO>()
                .ForMember(bd => bd.Authors, opt => opt.MapFrom(b => b.BookAuthors))
                .AfterMap((b, bd) =>
                 {
                     int count = 0;

                     foreach (var author in b.BookAuthors)
                     {
                         bd.Authors[count] = $"{author.Author.FirstName} {author.Author.LastName}";
                         count++;
                     }
                 });

            CreateMap<BookCreateDTO, Book>();

            CreateMap<BookUpdateDTO, Book>();

        }


    }
}
